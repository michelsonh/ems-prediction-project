# EMS Prediction Project

Repository containing all code work for our paper regarding EMS predictions in Milwaukee.

Authors: Alexandre Clavel, Hannah Michelson

## Table of Contents
2015_ems.csv - CSV of calls from 2015

2016_ems.csv - CSV of calls from 2016

2017_ems.csv - CSV of calls from 2017

2018_ems.csv - CSV of calls from 2018

2019_ems.csv - CSV of calls from 2019

2020_ems.csv - CSV of calls from 2020

2021_ems.csv - CSV of calls from 2021

column_descrition.csv - CSV of included census data columns

Data_Cleaning.ipynb - Jupyter notebook of data cleaning performed on EMS calls CSV's

ems_data.csv - Merged and cleaned EMS calls dataset

geckodriver - FireFox driver to allow rendering of maps

geckodriver.log - Logs outputted by geckodriver

info_by_zip_code.csv - CSV of cleaned census data sorted by zip codes included in EMS dataset

map.html - Temporary HTML file to display created maps

map_images - Directery containing rendered images of maps

mfdems.csv - CSV of calls from January to March 2021

MKE-zips.json - JSON containing coordinate information outlining Milwaukee zip codes

model.ipynb - Jupyter notebook containing mismatch model

preliminary_model.ipynb - Jupyter notebook containing predictive model

Summary Analysis.ipynb - Jupyter notebook detailing preliminary statistics of the combined dataset

us-zip-codes.geojson - GEOJSON file containing coordinate information for all zip codes

wi_wisconsin_zip_codes_geo.min.json - JSON file containing coordinate information in Wisconsin

zip codes.ipynb - Jupyter notebook containing cleaning of census data